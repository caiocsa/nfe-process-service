package br.com.nfe.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.nfe.services.NotaFiscalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotaFiscalBatch {

	private final NotaFiscalService service;

	@Scheduled(fixedRate = 120000 / 6)
	public void processarNotaFiscal() {
		try {
			service.processarNotaFiscal();
			log.info("Processo batch de nota fiscal realizado com sucesso");
		} catch (Exception e) {
			log.error("Falha no processo batch de nota fiscal.");
		}
	}
}
package br.com.nfe.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.nfe.model.NotaFiscal;
import br.com.nfe.model.StatusProcessamento;

@Repository
public interface NotaFiscalRepository extends JpaRepository<NotaFiscal, Long> {

	List<NotaFiscal> findByStatusProcessamento(StatusProcessamento statusProcessamento);
	
	NotaFiscal findByNumero(Long numero);
}
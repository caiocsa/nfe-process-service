package br.com.nfe.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class NotaFiscalDuplicata {

	@ApiModelProperty(value = "Identificador Nota Fiscal", name = "id", dataType = "Long", example = "123")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ApiModelProperty(value = "Parcela", name = "parcela", dataType = "Long", example = "123")
	@NotNull
	@Column(name = "PARCELA")
	private Long parcela;

	@ApiModelProperty(value = "Valor da Nota Fiscal", name = "valorNota", dataType = "BigDecimal", example = "João")
	@NotNull
	@Positive
	@Column(name = "VALOR")
	private String valor;

	@ApiModelProperty(value = "Data vencimento Nota Fiscal", name = "dataVencimento", dataType = "Date")
	@NotNull
	@Column(name = "DATA_VENCIMENTO")
	private Date dataVencimento;

	@OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "ID_NOTA_FISCAL", referencedColumnName = "ID")
	private NotaFiscal notaFiscal;
}
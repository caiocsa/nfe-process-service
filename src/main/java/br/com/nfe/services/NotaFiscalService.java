package br.com.nfe.services;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.nfe.exception.IntegrationException;
import br.com.nfe.model.NotaFiscal;
import br.com.nfe.model.StatusProcessamento;
import br.com.nfe.repository.NotaFiscalRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class NotaFiscalService {

	private final ArquivoService arquivoService;
	private final NotaFiscalRepository repository;

	public NotaFiscal processarNotaFiscal() throws IntegrationException, IOException {

		List<File> list = arquivoService.carregarTodosArquivos();
		list.stream().forEach(file -> {
			NotaFiscal nf = new NotaFiscal();
			try {
				nf = arquivoService.convertXmlToPojo(file);
				if (nf.getNumero() != null) {
					nf = repository.findByNumero(nf.getNumero());
					if (nf != null) {
						nf.setStatusProcessamento(StatusProcessamento.EM_PROCESSAMENTO);
						repository.save(nf);

						arquivoService.salvarArquivoOutput(file);
						file.delete();

						nf.setStatusProcessamento(StatusProcessamento.PROCESSADA);
						repository.save(nf);
					} else {
						arquivoService.salvarArquivoError(file);
						file.delete();
					}
				}
			} catch (IntegrationException e) {
				if (nf.getId() != null) {
					nf.setStatusProcessamento(StatusProcessamento.PROCESSADA_COM_ERRO);
					repository.save(nf);
				}
				arquivoService.salvarArquivoError(file);
			}
		});

		return null;
	}
}
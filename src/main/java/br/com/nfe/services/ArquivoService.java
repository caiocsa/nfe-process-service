package br.com.nfe.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import br.com.nfe.exception.IntegrationException;
import br.com.nfe.model.NotaFiscal;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ArquivoService {

	@Value("${diretorio.raiz}")
	private String raiz;

	@Value("${diretorio.input-nf}")
	private String diretorioInput;

	@Value("${diretorio.output-nf}")
	private String diretorioOutput;

	@Value("${diretorio.error-nf}")
	private String diretorioError;

	public void salvarArquivoOutput(File arquivo) throws IntegrationException {
		try {
			this.salvar(this.diretorioOutput, arquivo);
		} catch (Exception e) {
			throw new IntegrationException("Falha ao salvar o arquivo xml em disco.");
		}
	}

	public void salvarArquivoError(File arquivo) {
		try {
			this.salvar(this.diretorioError, arquivo);
		} catch (Exception e) {
			log.error("Falha ao salvar arquivo na pasta de Error");
		}
	}

	private void salvar(String diretorio, File arquivo) throws IOException {
		Path diretorioPath = Paths.get(this.raiz, diretorio);
		Path arquivoPath = diretorioPath.resolve(arquivo.getName());
		Files.createDirectories(diretorioPath);
		MultipartFile result = new MockMultipartFile(arquivo.getName(), new FileInputStream(arquivo));
		result.transferTo(arquivoPath.toFile());
	}

	public List<File> carregarTodosArquivos() throws IntegrationException {
		List<File> list = new ArrayList<>();
		try (Stream<Path> paths = Files.walk(Paths.get(raiz + "/" + diretorioInput))) {
			paths.filter(Files::isRegularFile).forEach(p -> {
				try {
					list.add(p.toFile());
				} catch (Exception e) {
					log.error("Falha ao acessar o arquivo : " + p.toString());
				}
			});
		} catch (IOException e1) {
			throw new IntegrationException("Falha ao acessar diretorio de input.");
		}
		return list;
	}

	public NotaFiscal convertXmlToPojo(File arquivo) throws IntegrationException {
		if (!arquivo.getName().endsWith(".xml")) {
			throw new IntegrationException("Formato do Arquivo deve ser XML");
		}
		try {
			DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = docBuilder.parse(new FileInputStream(arquivo));
			org.w3c.dom.Element varElement = document.getDocumentElement();
			JAXBContext context = JAXBContext.newInstance(NotaFiscal.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			JAXBElement<NotaFiscal> loader = unmarshaller.unmarshal(varElement, NotaFiscal.class);
			return loader.getValue();
		} catch (Exception e) {
			throw new IntegrationException("Falha ao realizar leitura do arquivo xml.");
		}
	}
}